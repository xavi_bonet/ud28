var valores = [true, 5, false, "hola", "adios", 2];
var texto = [];
var numero = [];

var lengthString = 0;
var posicionDelString = 0;

var i = 0;
while (i < valores.length) {
    if (typeof valores[i] == 'string') {
        texto.push(valores[i]);
    }
    if (typeof valores[i] == 'number') {
        numero.push(valores[i]);
    }
    if (typeof valores[i] == 'boolean') {
        console.log(!valores[i]);
    }
    i++;
}

if (texto[0].length > texto[1].length) {
    console.log(texto[0] + " > " + texto[1]);
} else {
    console.log(texto[0] + " < " + texto[1]);
}

console.log("Numeros " + numero[0] + " " + numero[1]);
console.log("Suma " + (numero[0] + numero[1]));
console.log("Resta " + (numero[0] - numero[1]));
console.log("Multiplicacion " + (numero[0] * numero[1]));
console.log("Division " + (numero[0] / numero[1]));
console.log("Modulo " + (numero[0] % numero[1]));
