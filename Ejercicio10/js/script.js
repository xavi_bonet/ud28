window.onload = function(){
    
    var button = document.getElementById('enviar').onclick = msg;

    function msg() {
        if(esPalindromo()){
            alert('Es un palindromo');
        }else{
            alert('No es un palindromo');
        }
    }

    function esPalindromo(){
        var entrada = document.getElementById('txt').value;
        var re = /[^A-Za-z0-9]/g;
        entrada = entrada.toLowerCase().replace(re, '');
        var entradaReversed = entrada.split('').reverse().join(''); 
        return entradaReversed === entrada;
    }
}

