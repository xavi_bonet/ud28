![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD28

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Team Member | 11/03/2021 |   |   |  |
| David Bonet Daga |  | Team Member | 11/03/2021 |   |   |  |

#### 2. Description
```
Ejercicios UD28
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud28.git

```
UD28 / https://gitlab.com/xavi_bonet/ud28.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Visual Studio Code
--------
Plugins:
vscode-icons
Beautify
markdown-preview
live server
```
